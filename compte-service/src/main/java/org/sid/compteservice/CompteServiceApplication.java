package org.sid.compteservice;

import java.util.Date;

import org.sid.compteservice.entities.Compte;
import org.sid.compteservice.enums.TypeCompte;
import org.sid.compteservice.repositories.CompteRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class CompteServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompteServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(CompteRepository compteRepository, RepositoryRestConfiguration restConfiguration) {
		return args -> {
			// Modification configuration par défaut de spring-data-rest pour exposer les id de la classe Compte
			restConfiguration.exposeIdsFor(Compte.class);
			
			// Ajout d'éléments dans la bdd h2
			compteRepository.save(new Compte(null, 98000, new Date(), TypeCompte.COURANT));
			compteRepository.save(new Compte(null, 12000, new Date(), TypeCompte.EPARGNE));
			compteRepository.save(new Compte(null, 2100, new Date(), TypeCompte.COURANT));

			// Debug pour afficher les elts présents en bdd
			compteRepository.findAll().forEach(cp -> {
				System.out.println(cp.toString());
			});
		};		
	}

}
